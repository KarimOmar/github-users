# Github Users

Based on _AngularJS_ , _Grunt_ , _Foundation_ and _Yeoman_

## Installation
Provided that **Nodejs** and **Ruby** are already installed

- npm install -g grunt-cli bower yo generator-karma generator-angular karma jshint
- gem install compass
- npm install
- bower install

## Build & development

- Run `grunt` for building
- Run `grunt serve` for serving and preview.
