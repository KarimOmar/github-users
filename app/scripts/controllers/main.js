"use strict";

/**
 * @ngdoc function
 * @name instabugApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the instabugApp
 */

(function() {

  function controller(scope) {

  }
  angular.module("instabugApp")
    .controller("MainCtrl", ["$scope", controller]);
}).call(null);
