"use strict";

/**
 * @ngdoc function
 * @name instabugApp.controller:UsersCtrl
 * @description
 * # UsersCtrl
 * Controller of the instabugApp
 */

(function() {

  function controller(scope, getUsers, $http) {
    scope.users = getUsers;

    scope.viewUserInfo = function viewUserInfo(login) {
      if (!login) {
        return;
      }
      var userLogin = login;
      $http.get("https://api.github.com/users/" + userLogin)
        .then(function(response) {
          scope.selectedUser = response.data;
          console.log(scope.selectedUser);
        });
    };

    scope.loadMore = function loadMore() {
      scope.isLoading = true;
      var lastUser = _.last(scope.users);
      $http.get("https://api.github.com/users?per_page=20&since=" + lastUser.id)
        .then(function(response) {
          scope.users = _.union(scope.users, response.data);
          scope.isLoading = false;
        });
    };

    function init() {
      var firstUser = _.head(scope.users);
      scope.viewUserInfo(firstUser.login);
    }
    init();
  }
  angular.module("instabugApp")
    .controller("UsersCtrl", ["$scope", "getUsers", "$http", controller]);
}).call(null);
