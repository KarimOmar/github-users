"use strict";

/**
 * @ngdoc function
 * @name instabugApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the instabugApp
 */

(function() {

  function controller(scope) {
    
  }
  angular.module("instabugApp")
    .controller("AboutCtrl", ["$scope", controller]);
}).call(null);
