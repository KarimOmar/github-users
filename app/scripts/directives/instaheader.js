"use strict";

/**
 * @ngdoc directive
 * @name instabugApp.directive:instaheader
 * @description
 * # instaheader
 */

(function() {

  function controller(scope) {
    $(document).foundation();
  }
  angular.module("instabugApp")
    .directive("instaheader", function() {
      return {
        templateUrl: "views/directives/instaheader.html",
        restrict: "E",
        replace: true,
        controller: ["$scope", controller]
      };
    });
}).call(null);
