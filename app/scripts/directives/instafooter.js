"use strict";

/**
 * @ngdoc directive
 * @name instabugApp.directive:instafooter
 * @description
 * # instafooter
 */

(function() {

  function controller(scope) {
    $(document).foundation();
  }
  angular.module("instabugApp")
    .directive("instafooter", function() {
      return {
        templateUrl: "views/directives/instafooter.html",
        restrict: "E",
        replace: true,
        controller: ["$scope", controller]
      };
    });
}).call(null);
