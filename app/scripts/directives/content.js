"use strict";

/**
 * @ngdoc directive
 * @name instabugApp.directive:content
 * @description
 * # content
 */
(function() {

  function controller() {

  }
  angular.module("instabugApp")
    .directive("content", function() {
      return {
        templateUrl: "views/directives/content.html",
        restrict: "E",
        transclude: true,
        controller: ["$scope", controller]
      };
    });
}).call(null);
