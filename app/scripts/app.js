"use strict";

/**
 * @ngdoc overview
 * @name instabugApp
 * @description
 * # instabugApp
 *
 * Main module of the application.
 */
var modules = [
  'ngAnimate',
  'ngAria',
  'ngCookies',
  'ngMessages',
  'ngResource',
  'ngRoute',
  'ui.router',
  'ngSanitize',
  'ngTouch',
  'mm.foundation'
];
var instabugApp = angular.module("instabugApp", modules);
instabugApp.config(["$compileProvider", "$stateProvider", "$urlRouterProvider",
  function($compileProvider, $stateProvider, $urlRouterProvider) {

    // Angular Debug Info Disable
    $compileProvider.debugInfoEnabled(false);

    $urlRouterProvider.otherwise('/');

    $stateProvider
      .state('main', {
        url: '/',
        controller: 'MainCtrl',
        templateUrl: 'views/main.html',
        data: {
          pageTitle: "Home",
          pageDescription: "Welcome to Github users, The first platform for exploring our talented developers"
        }
      })

      .state('about', {
        url: '/about',
        controller: 'AboutCtrl',
        templateUrl: 'views/about.html',
        data: {
          pageTitle: "About Us",
          pageDescription: "The story behind Github users and the talented team"
        }
      })

      .state('users', {
        url: '/users',
        controller: 'UsersCtrl',
        templateUrl: 'views/users.html',
        data: {
          pageTitle: "Users",
          pageDescription: "Find and explore our talented developers"
        },
        resolve: {
          getUsers: ['$http', function($http) {
            return $http
              .get("https://api.github.com/users?per_page=20")
              .then(function(response){
                  console.log(response.data)
                  return response.data;
              })
          }]
        },
      });
  }
]);

instabugApp.run(["$rootScope", "$route", "$location", "$state", "$transitions",
  function(root, $route, $location, $state, $transitions) {
    // Set Page Title from Route
    root.setTitle = function(pageTitle) {
      var formatTitle = pageTitle + " :: Github Users";
      root.pageTitle = formatTitle;
    };

    // Set Page Description from Route
    root.setDescription = function(pageDescription) {
      root.pageDescription = pageDescription;
    };

    // Routing Watcher
    $transitions.onStart( {}, function($transition$) {
      var stateDetails = $transition$.$to();
      console.log("Page: " + stateDetails.url.pattern);
      root.setTitle(stateDetails.data.pageTitle);
      root.setDescription(stateDetails.data.pageDescription);
    });

  }
]);
